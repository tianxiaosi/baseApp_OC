//
//  UIColor+UItil.h
//  Pinla_ios_2.0
//
//  Created by 10Lab on 15/12/1.
//  Copyright © 2015年 10lab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (UItil)

+ (UIColor *)colorWithHexString:(NSString *)stringToConvert;

+ (UIColor *)colorWithHexString:(NSString *)stringToConvert alpha:(CGFloat)alpha;

/**
 *  根据品质返回颜色值
 */
+ (UIColor *)colorWithQuality:(NSInteger)quality;

/**
 *  根据品质返回道具描述内容颜色值
 */
+ (UIColor *)colorDescTextWithQuality:(NSInteger)quality;

/**
 *  新版根据品质返回颜色，顺序反的
 */
+ (UIColor *)colorWithNewQuality:(NSInteger)quality;
/**
 *  根据品质返回颜色
 *
 *  @param quality 品质
 *  @param alpha   alpha
 *
 *  @return <#return value description#>
 */

+ (UIColor *)colorWithQuality:(NSInteger)quality andAlpha:(CGFloat)alpha;



@end
