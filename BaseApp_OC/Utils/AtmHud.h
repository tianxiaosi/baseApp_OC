//
//  AtmHud.h
//  ICOCommon
//
//  Created by tianlele on 2017/10/9.
//  Copyright © 2017年 Tianxiaosi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AtmHud : NSObject
+(void)showLoading;

+(void)hideLoading;

+(void)showLoadingWithTip:(NSString *)tip;

// time  = 1.5
+(void)showTip:(NSString *)tip;

+(void)showTip:(NSString *)tip time:(NSTimeInterval)time;
@end
