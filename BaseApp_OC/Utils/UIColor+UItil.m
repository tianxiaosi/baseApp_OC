//
//  UIColor+UItil.m
//  Pinla_ios_2.0
//
//  Created by 10Lab on 15/12/1.
//  Copyright © 2015年 10lab. All rights reserved.
//

#import "UIColor+UItil.h"
#define DEFAULT_VOID_COLOR [UIColor whiteColor]
@implementation UIColor (UItil)

+ (UIColor *)colorWithHexString:(NSString *)stringToConvert
{
    
    return [UIColor colorWithHexString:stringToConvert alpha:1.0];
}

+ (UIColor *)colorWithHexString:(NSString *)stringToConvert alpha:(CGFloat)alpha
{
    // 删除里面含有的字符集合  去除2端空格 转换成大写字符串
    NSString * cString = [[stringToConvert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    if ([cString length] < 6)
        return DEFAULT_VOID_COLOR;
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return DEFAULT_VOID_COLOR;
    
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];

    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:alpha];
    
}

/**
 *  根据品质返回颜色值
 */

+ (UIColor *)colorWithQuality:(NSInteger)quality andAlpha:(CGFloat)alpha
{
    
    NSString * colorString = nil;
    switch (quality) {
        case 0:
            return [UIColor whiteColor];
            break;
        case 1:
            colorString =  @"999999"; //普通
            break;
        case 2:
            colorString =  @"90d618"; // 罕见
            break;
            
        case 3:
            colorString =  @"33e6d7"; // 稀有
            break;
            
        case 4:
            colorString =  @"2b95ff"; // 神话
            break;
            
        case 5:
            colorString =  @"ce3cf5";  // 传说
            break;
        case 6:
            colorString =  @"f8e333"; // 不朽
            break;
        case 7:
            colorString =  @"ff9813"; // 至宝
            break;
            
            
        default:
            break;
    }
    UIColor * color  = [UIColor colorWithHexString:colorString alpha:alpha];
    return color;

    
}

+ (UIColor *)colorWithQuality:(NSInteger)quality
{
    NSString * colorString = nil;
    switch (quality) {
        case 0:
            return [UIColor whiteColor];
            break;
        case 1:
            colorString =  @"999999"; //普通
            break;
        case 2:
            colorString =  @"90d618"; // 罕见
            break;
            
        case 3:
            colorString =  @"33e6d7"; // 稀有
            break;
            
        case 4:
            colorString =  @"2b95ff"; // 神话
            break;
            
        case 5:
            colorString =  @"ce3cf5";  // 传说
            break;
        case 6:
            colorString =  @"f8e333"; // 不朽
            break;
        case 7:
            colorString =  @"ff9813"; // 至宝
            break;
            
            
        default:
            break;
    }
    UIColor * color  = [UIColor colorWithHexString:colorString];
    return color;
}

/**
 *  根据品质返回道具描述内容颜色值
 */
+ (UIColor *)colorDescTextWithQuality:(NSInteger)quality
{
    NSString * colorString = nil;
    switch (quality) {
        case 0:
            return [UIColor whiteColor];
            break;
        case 1:
            colorString =  @"c0c0c0"; //普通
            break;
        case 2:
            colorString =  @"98c684"; // 罕见
            break;
            
        case 3:
            colorString =  @"84c2c6"; // 稀有
            break;
            
        case 4:
            colorString =  @"84a3c6"; // 神话
            break;
            
        case 5:
            colorString =  @"b884c6";  // 传说
            break;
        case 6:
            colorString =  @"c6b984"; // 不朽
            break;
        case 7:
            colorString =  @"c69784"; // 至宝
            break;
            
            
        default:
            break;
    }
    UIColor * color  = [UIColor colorWithHexString:colorString];
    return color;
}

+ (UIColor *)colorWithNewQuality:(NSInteger)quality
{
    NSString * colorString = nil;
    switch (quality) {
        case 0:
            return [UIColor whiteColor];
            break;
        case 7:
            colorString =  @"999999"; //普通
            break;
        case 6:
            colorString =  @"90d618"; // 罕见
            break;
            
        case 5:
            colorString =  @"33e6d7"; // 稀有
            break;
            
        case 4:
            colorString =  @"2b95ff"; // 神话
            break;
            
        case 3:
            colorString =  @"ce3cf5";  // 传说
            break;
        case 2:
            colorString =  @"f8e333"; // 不朽
            break;
        case 1:
            colorString =  @"ff9813"; // 至宝
            break;
        default:
            break;
    }
    UIColor * color  = [UIColor colorWithHexString:colorString];
    
    
    return color;
    
}


@end
