//
//  NSString+TimestampToDate.h
//  ICOCommon
//
//  Created by tianlele on 2017/8/28.
//  Copyright © 2017年 Tianxiaosi. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,DateType) {
    yyyyMMdd = 0,
};

// 时间戳转日期
@interface NSString (TimestampToDate)

+(NSString *)dateWithTimetamp:(NSString *)timetamp;

@end
