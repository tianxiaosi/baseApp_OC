//
//  NSString+TimestampToDate.m
//  ICOCommon
//
//  Created by tianlele on 2017/8/28.
//  Copyright © 2017年 Tianxiaosi. All rights reserved.
//

#import "NSString+TimestampToDate.h"

@implementation NSString (TimestampToDate)
+(NSString *)dateWithTimetamp:(NSString *)timetamp{
    NSDateFormatter*formatter = [[NSDateFormatter alloc]init];
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"MM-dd HH:mm"];
    
    NSDate*timeDate = [NSDate dateWithTimeIntervalSince1970:[timetamp floatValue]];
    
    NSString*confromTimespStr = [formatter stringFromDate:timeDate];
    
    return confromTimespStr;
}
@end
