//
//  UserStorage.h
//  PinLa-IOS
//
//  Created by lixiao on 15/4/17.
//  Copyright (c) 2015年 tenTab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDefaultsUtils.h"

@interface UserStorage : NSObject
//本地多语言
+(void)saveAppLanguage:(NSString *)appLan;
+(NSString *)appLanguage;

+(void)saveUserId:(NSString *)userId;
+(NSString *)userId;

+(void)userName:(NSString *)userName;
+(NSString *)userName;

+(void)phoneNum:(NSString *)phoneNum;
+(NSString *)phoneNum;

+(void)token:(NSString *)token;
+(NSString *)token;

+(void)certification:(NSString *)cer;
+(NSString *)certification;

+(void)phone_prefix:(NSString *)phonePrefix;
+(NSString *)phonePrefix;

+(void)userCerProgress:(NSInteger)progress;
+(NSInteger)userCerProgress;

+(void)companyCerProgress:(NSInteger)progress;
+(NSInteger)companyCerProgress;

+(void)userLoginTime:(NSInteger)time;
+(NSInteger)userLoginTime;

// 当前汇率类型
+(void)currencyType:(NSString *)type;
+(NSString *)currencyType;

+(void)clearUserStorage;
@end
