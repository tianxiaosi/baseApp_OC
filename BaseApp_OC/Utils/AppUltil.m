//
//  AppUltil.m
//  ICOCommon
//
//  Created by tianlele on 2017/9/26.
//  Copyright © 2017年 Tianxiaosi. All rights reserved.
//

#import "AppUltil.h"

@implementation AppUltil
+ (BOOL)isChinaMobile:(NSString *)phoneNum
{
    NSString *CM = @"(^1(3[4-9]|4[7]|5[0-27-9]|7[8]|8[2-478])\\d{8}$)|(^1705\\d{7}$)";
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    return [regextestcm evaluateWithObject:phoneNum];
}

+ (BOOL)validatePassword:(NSString*)passWord
{
    NSString *pattern = @"^(?![0-9]+$)(?![a-zA-Z]+$)[a-zA-Z0-9]{6,20}";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:passWord];
    return isMatch;
}

+ (BOOL)validateUrl:(NSString *)urlStr{
    NSString * urlRegex = @"(https?|ftp|file)://[-A-Za-z0-9+&@#/%?=~_|!:,.;]+[-A-Za-z0-9+&@#/%=~_|]";
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",urlRegex];
    BOOL isUrl = [predicate evaluateWithObject:urlStr];
    return isUrl;
}

+(NSString *)imageToBase64String:(UIImage *)image{
    
    NSData *imagedata = UIImageJPEGRepresentation(image, 0.1f);
    
    NSString *image64 = [imagedata base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    return image64;
}

+ (UIImage *)base64StringToImage:(NSString *)str {
    
    NSData * imageData =[[NSData alloc] initWithBase64EncodedString:str options:NSDataBase64DecodingIgnoreUnknownCharacters];
    
    UIImage *photo = [UIImage imageWithData:imageData ];
    
    return photo;
    
}

+(NSString *)cerGetDefaultCountry{
        NSString *language = [UserStorage appLanguage];
        if ([language hasPrefix:@"zh-Hans"]) {
            return @"中国";
        } else if ([language hasPrefix:@"ja"]) {
            return @"日本";
        } else if ([language hasPrefix:@"en"]) {
            return @"美国";
        }else{
           return @"中国";
        }
}

+(CGFloat)widthWithString:(NSString *)string font:(UIFont *)font{
    NSDictionary *attrs = @{NSFontAttributeName:font};
    CGSize size=[string sizeWithAttributes:attrs];
    return size.width;
}

+(NSString *)certificationStatue{
    switch ([[UserStorage certification] integerValue]) {
        case 0:
            return @"未认证";
            break;
        case 1:
            return @"通过个人认证";
            break;
        case 2:
            return @"通过企业认证";
            break;
        case 3:
            return @"等待邮包";
            break;
        case 4:
            return @"审核中";
            break;
        case 5:
            return @"审核拒绝";
            break;
        default:
            break;
    }
    return @"未认证";
}

+ (NSString *)certificationMineStatue{
    switch ([[UserStorage certification] integerValue]) {
        case 0:
            return @"未认证";
            break;
        case 1:
            return @"已认证";
            break;
        case 2:
            return @"已认证";
            break;
        case 3:
            return @"审核中";
            break;
        case 4:
            return @"审核中";
            break;
        case 5:
            return @"未认证";
            break;
        default:
            break;
    }
    return @"未认证";
}

+ (BOOL)stringNullOrEmpty:(NSString *)string{
    if (!string) {
        return YES;
    }else if(string.length == 0){
        return YES;
    }else{
        return NO;
    }
    return NO;
}

/**
 * 计算文字高度，可以处理计算带行间距的
 */
+(CGSize)boundingRectWithSize:(CGSize)size font:(UIFont*)font  lineSpacing:(CGFloat)lineSpacing string:(NSString *)str
{
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:str];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = lineSpacing;
    [attributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, str.length)];
    [attributeString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0,str.length)];
    NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    CGRect rect = [attributeString boundingRectWithSize:size options:options context:nil];
    
    NSLog(@"size:%@", NSStringFromCGSize(rect.size));
    
    //文本的高度减去字体高度小于等于行间距，判断为当前只有1行
    if ((rect.size.height - font.lineHeight) <= paragraphStyle.lineSpacing) {
        if ([self containChinese:str]) {  //如果包含中文
            rect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height-paragraphStyle.lineSpacing);
        }
    }
    
    return rect.size;
}


/**
 根据文字宽度，获取高度
 */
+(CGFloat)heightWithString:(NSString *)string font:(UIFont *)font width:(CGFloat)width{
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, 0)];
    label.numberOfLines = 0;
    label.font = font;
    label.text = string;
    [label sizeToFit];
    return CGRectGetHeight(label.frame);
}


//判断如果包含中文
+ (BOOL)containChinese:(NSString *)str {
    for(int i=0; i< [str length];i++){ int a = [str characterAtIndex:i];
        if( a > 0x4e00 && a < 0x9fff){
            return YES;
        }
    }
    return NO;
}

@end
