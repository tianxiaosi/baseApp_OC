//
//  UserStorage.m
//  PinLa-IOS
//
//  Created by lixiao on 15/4/17.
//  Copyright (c) 2015年 tenTab. All rights reserved.
//  用户仓库

#import "UserStorage.h"

static NSString * const APPLANGUAGE = @"APPLANGUAGE";
static NSString * const USER_ID = @"USER_ID";
static NSString * const USER_NAME = @"USER_NAME";
static NSString * const CERTIFICATION = @"CERTIFICATION";
static NSString * const PHONE_NUM = @"PHONE_NUM";
static NSString * const PHONE_PREFIX = @"PHONE_PREFIX";
static NSString * const TOKEN = @"TOKEN";
static NSString * const USER_CER_PROGRESS = @"USER_CER_PROGRESS";
static NSString * const COMPANY_CER_PROGRESS = @"COMPANY_CER_PROGRESS";
static NSString * const USER_LOGIN_TIME = @"USER_LOGIN_TIME";
static NSString * const CURRENCY_TYPE = @"CURRENCY_TYPE"; // 当前汇率类型

@implementation UserStorage

+(void)clearUserStorage{
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];

    NSArray * userArr = @[USER_ID,USER_NAME,CERTIFICATION,PHONE_NUM,PHONE_PREFIX,TOKEN,USER_CER_PROGRESS,COMPANY_CER_PROGRESS];
    
    for(id key in userArr) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
}

//本地多语言
+(void)saveAppLanguage:(NSString *)appLan{
    [UserDefaultsUtils saveValue:appLan forKey:APPLANGUAGE];
}

+(NSString *)appLanguage{
   return [UserDefaultsUtils valueWithKey:APPLANGUAGE];
}

+(void)saveUserId:(NSString *)userId{
    [UserDefaultsUtils saveValue:userId forKey:USER_ID];
}
+(NSString *)userId{
   return [UserDefaultsUtils valueWithKey:USER_ID];
}

+(void)userName:(NSString *)userName{
    [UserDefaultsUtils saveValue:userName forKey:USER_NAME];
}
+(NSString *)userName{
   return [UserDefaultsUtils valueWithKey:USER_NAME];
}

+(void)phoneNum:(NSString *)phoneNum{
    [UserDefaultsUtils saveValue:phoneNum forKey:PHONE_NUM];
}
+(NSString *)phoneNum{
    return [UserDefaultsUtils valueWithKey:PHONE_NUM];
}

+(void)token:(NSString *)token{
    [UserDefaultsUtils saveValue:token forKey:TOKEN];
}
+(NSString *)token{
    return [UserDefaultsUtils valueWithKey:TOKEN];
}

+(void)certification:(NSString *)cer{
    [UserDefaultsUtils saveValue:cer forKey:CERTIFICATION];
}
+(NSString *)certification{
    return [UserDefaultsUtils valueWithKey:CERTIFICATION];
}

+(void)phone_prefix:(NSString *)phonePrefix{
    [UserDefaultsUtils saveValue:phonePrefix forKey:PHONE_PREFIX];
}
+(NSString *)phonePrefix{
    return [UserDefaultsUtils valueWithKey:PHONE_PREFIX];
}

+(void)userCerProgress:(NSInteger)progress{
    [UserDefaultsUtils saveInteger:progress forKey:USER_CER_PROGRESS];
}
+(NSInteger)userCerProgress{
    if ([UserDefaultsUtils integerWithKey:USER_CER_PROGRESS] < 1) {
        return 0;
    }
    return [UserDefaultsUtils integerWithKey:USER_CER_PROGRESS];
}

+(void)companyCerProgress:(NSInteger)progress{
    [UserDefaultsUtils saveInteger:progress forKey:COMPANY_CER_PROGRESS];
}
+(NSInteger)companyCerProgress{
    if ([UserDefaultsUtils integerWithKey:COMPANY_CER_PROGRESS] < 1) {
        return 0;
    }
    return [UserDefaultsUtils integerWithKey:COMPANY_CER_PROGRESS];
}

+(void)userLoginTime:(NSInteger)time{
    [UserDefaultsUtils saveInteger:time forKey:USER_LOGIN_TIME];
}

+(NSInteger)userLoginTime{
    return [UserDefaultsUtils integerWithKey:USER_LOGIN_TIME];
}
// 当前汇率类型
+(void)currencyType:(NSString *)type{
    [UserDefaultsUtils saveValue:type forKey:CURRENCY_TYPE];
}

+(NSString *)currencyType{
    return [UserDefaultsUtils valueWithKey:CURRENCY_TYPE];
}

@end
