//
//  AppUltil.h
//  ICOCommon
//
//  Created by tianlele on 2017/9/26.
//  Copyright © 2017年 Tianxiaosi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AppUltil : NSObject

+ (BOOL)isChinaMobile:(NSString *)phoneNum;

+ (BOOL)validatePassword:(NSString*)passWord;

+ (BOOL)validateUrl:(NSString *)urlStr;

// 图片转base64
+(NSString *)imageToBase64String:(UIImage *)image;

+ (UIImage *)base64StringToImage:(NSString *)str;

+ (NSString *)cerGetDefaultCountry;

/**
 * 计算文字高度，可以处理计算带行间距的
 */
+(CGSize)boundingRectWithSize:(CGSize)size font:(UIFont*)font lineSpacing:(CGFloat)lineSpacing string:(NSString *)str;

/**
 根据文字宽度，获取高度
 */
+(CGFloat)heightWithString:(NSString *)string font:(UIFont *)font width:(CGFloat)width;

+(CGFloat)widthWithString:(NSString *)string font:(UIFont *)font;

+(NSString *)certificationStatue;

+ (NSString *)certificationMineStatue;

+ (BOOL)stringNullOrEmpty:(NSString *)string;
@end
