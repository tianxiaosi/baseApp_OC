//
//  AtmHud.m
//  ICOCommon
//
//  Created by tianlele on 2017/10/9.
//  Copyright © 2017年 Tianxiaosi. All rights reserved.
//

#import "AtmHud.h"

@implementation AtmHud

+(void)showLoading{
    [MBProgressHUD showHUDAddedTo:VIEW_WINDOW animated:YES];
}

+(void)hideLoading{
    [MBProgressHUD hideHUDForView:VIEW_WINDOW animated:YES];
}

+(void)showLoadingWithTip:(NSString *)tip{
    [MBProgressHUD hideHUDForView:VIEW_WINDOW animated:YES];
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:VIEW_WINDOW animated:YES];
    hud.detailsLabel.text = ATMLocalizedString(tip);
//    hud.removeFromSuperViewOnHide = YES;
//    [hud hideAnimated:YES afterDelay:time];
}

+(void)showTip:(NSString *)tip time:(NSTimeInterval)time{
    [MBProgressHUD hideHUDForView:VIEW_WINDOW animated:YES];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:VIEW_WINDOW animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.detailsLabel.text = ATMLocalizedString(tip);
    hud.margin = 10.f;
    //       hud.offset.y = 150.f;
    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:time];
}

+(void)showTip:(NSString *)tip{
    [MBProgressHUD hideHUDForView:VIEW_WINDOW animated:YES];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:VIEW_WINDOW animated:YES];
    hud.bezelView.backgroundColor = [UIColor blackColor];
    hud.mode = MBProgressHUDModeText;
    hud.detailsLabel.text = ATMLocalizedString(tip);
    hud.detailsLabel.textColor = [UIColor whiteColor];
    hud.margin = 10.f;
    //       hud.offset.y = 150.f;
    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:1.5f];
}

@end
