//
//  BaseViewController.h
//  CommonApp
//
//  Created by tianlele on 2017/7/5.
//  Copyright © 2017年 Tianxiaosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController
-(void)setCustomTitle:(NSString *)title;
@end
