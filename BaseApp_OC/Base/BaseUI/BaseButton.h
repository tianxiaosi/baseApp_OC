//
//  BaseButton.h
//  ICOCommon
//
//  Created by tianlele on 2017/9/25.
//  Copyright © 2017年 Tianxiaosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseButton : UIButton

-(instancetype)initWithFrame:(CGRect)frame withTitleKey:(NSString *)key;

-(void)setTitleKey:(NSString *)key forState:(UIControlState)state;

@end
