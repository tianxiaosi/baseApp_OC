//
//  BaseLabel.h
//  ICOCommon
//
//  Created by tianlele on 2017/9/25.
//  Copyright © 2017年 Tianxiaosi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM (NSInteger ,VerticalAlignment){
    VerticalAlignmentTop = 0,//上居中
    VerticalAlignmentMiddle,//中居中
    VerticalAlignmentBottom,//低居中
};

@interface BaseLabel : UILabel
@end
