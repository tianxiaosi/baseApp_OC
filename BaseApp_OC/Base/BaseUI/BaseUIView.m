//
//  BaseUIView.m
//  ICOCommon
//
//  Created by tianlele on 2017/9/25.
//  Copyright © 2017年 Tianxiaosi. All rights reserved.
//

#import "BaseUIView.h"

@implementation BaseUIView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction)];
        [self addGestureRecognizer:tap];
    }
    return self;
}


-(void)tapAction{
    [self endEditing:YES];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
