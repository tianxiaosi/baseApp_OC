//
//  BaseTextField.h
//  ICOCommon
//
//  Created by tianlele on 2017/10/9.
//  Copyright © 2017年 Tianxiaosi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTextField : UITextField

-(instancetype)initWithFrame:(CGRect)frame placeHoldKey:(NSString *)placeHoldKey;

-(void)setCustomPlaceholder:(NSString *)placeHoldKey;

-(void)setCustomText:(NSString *)textKey;

@end
