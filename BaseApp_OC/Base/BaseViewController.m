//
//  BaseViewController.m
//  CommonApp
//
//  Created by tianlele on 2017/7/5.
//  Copyright © 2017年 Tianxiaosi. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()
@property (nonatomic , strong) NSString * titleKey;

@property (nonatomic , strong) UIBarButtonItem * leftItem ;
@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"#f3f3f6"]];
    
    // Do any additional setup after loading the view.
}

-(void)setLocalizedString{
}

-(void)leftItemAction{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dealloc{
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
